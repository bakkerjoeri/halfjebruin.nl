define([
	'express'
], function (express) {
	var Server = {

		start: function () {

			var es = express();

			es.get('/', function (req, res) {
				res.send("We're live, people!");
			});

			es.listen(3000);
		}
	}

	return Server;

})