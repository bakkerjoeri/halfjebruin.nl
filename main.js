var requirejs = require('requirejs');

requirejs.config({
	paths: {
		
	},

	nodeRequire: require
});

requirejs([
	'lib/server'
], function (Server) {
	Server.start();
});